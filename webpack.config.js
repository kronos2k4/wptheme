
var path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var HtmlWebpackPlugin = require('html-webpack-plugin');
var webpack = require('webpack');


var sassLoaders = [
    "css-loader?sourceMap",
    "autoprefixer-loader?browsers=last 10 version",
    "sass-loader?indentedSyntax=sass&sourceMap=map&includePaths[]=" + path.resolve(__dirname, "./sass"),
];

module.exports = {
    entry: {
        app: './js/entry.js',
        vendor: ['react', 'jquery', 'lodash', 'react-router', 'history', 'firebase', 'fullpage.js', './jquery/jquery.slimscroll.js']
    },

    output: {
        path: "public",
        filename: "[name].js"
    },

    module: {
        loaders: [
            {
                test: /\.sass$/,
                loader: ExtractTextPlugin.extract("style-loader", sassLoaders.join("!"))
            },
            {
                test: /\.js?$/,
                exclude: /(node_modules)/,
                loader: 'babel',
                query:
                {
                   presets: ['es2015','stage-0','react']
                }
            },
            {
                test: /\.jpg$|.eot$|.ttf$|.woff$/,
                loader: 'file-loader'
            }
        ]
    },

    resolve: {
        extensions: ['', '.js', '.jsx'],
        modulesDirectories: ["./js", "node_modules"],
    },

    plugins: [
        new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.bundle.js'),
        new ExtractTextPlugin("index.css"),
        new HtmlWebpackPlugin({
            template : 'index.html',
            // hash     : true,
            filename : 'index.html',
            inject   : 'body'
        }),

    ]
};