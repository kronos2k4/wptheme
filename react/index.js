//REACT INIT
import React from 'react';
import ReactDOM from 'react-dom';

export default class Main extends React.Component {
    render() {
        return (
            <div id="fullpage">

                <div className="section mainPage">
                    <div className="logo">
                        PHOTOGALLERY
                    </div>
                </div>
                <div className="section gallery">
                    <div className="slide" data-anchor="slide1">

                        <div className="scroll-block">

                            <ul className="grid effect-2">
                                <li className="grid-item">
                                    <a href="#">
                                        <img src="./images/main_background_thumb.jpg" alt=""/>
                                    </a>
                                </li>
                                <li className="grid-item">
                                    <a href="#">
                                        <img src="./images/main_background_thumb.jpg" alt=""/>
                                    </a>
                                </li>
                                <li className="grid-item">
                                    <a href="#">
                                        <img src="./images/main_background_thumb.jpg" alt=""/>
                                    </a>
                                </li>
                                <li className="grid-item">
                                    <a href="#">
                                        <img src="./images/main_background_thumb.jpg" alt=""/>
                                    </a>
                                </li>
                                <li className="grid-item">
                                    <a href="#">
                                        <img src="./images/main_background_thumb.jpg" alt=""/>
                                    </a>
                                </li>
                                <li className="grid-item">
                                    <a href="#">
                                        <img src="./images/main_background_thumb.jpg" alt=""/>
                                    </a>
                                </li>
                                <li className="grid-item">
                                    <img data-src="./images/shore_thumb.jpg" alt=""/>
                                </li>
                                <li className="grid-item">
                                    <img data-src="./images/bridge_thumb.jpg" alt=""/>
                                </li>
                                <li className="grid-item">
                                    <img data-src="./images/detroit_thumb.jpg" alt=""/>
                                </li>
                                <li className="grid-item">
                                    <img data-src="./images/italy_thumb.jpg" alt=""/>
                                </li>
                                <li className="grid-item">
                                    <img data-src="./images/lighthouse_thumb.jpg" alt=""/>
                                </li>
                                <li className="grid-item">
                                    <img data-src="./images/mountain_thumb.jpg" alt=""/>
                                </li>
                                <li className="grid-item">
                                    <img data-src="./images/nature_thumb.jpg" alt=""/>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div className="slide" data-anchor="slide2">Two 2</div>

                </div>
                <div className="section contacts">Some section</div>
            </div>
        )
    }

}