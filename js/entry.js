'use strict';

require("../sass/style.sass");

//LODASH INIT
import _ from 'lodash';

import jqScripts from '../jquery/jqScripts';
import Main from '../react/index';

import React from 'react';
import ReactDOM from 'react-dom';

ReactDOM.render(<Main/>, document.getElementById('app'));
