import $ from 'jquery';

import fullpage from 'fullpage.js';
import Masonry from 'masonry-layout';
import slimScroll from './jquery.slimscroll.js';


var elem = document.querySelector('.grid');
var msnry = new Masonry( elem, {
    itemSelector: '.grid-item',
});

//var msnry = new Masonry( '.grid', {
//
//});

$(document).ready(function() {
    $('#fullpage').fullpage({
        keyboardScrolling: true,
        loopTop: true,
        loopBottom: true,
        fitToSection: true,
        sectionsColor: ['orange', 'orange', 'orange'],
        scrollOverflow: true,

    });

});